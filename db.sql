CREATE TABLE `aadhaar`.`ciudades` (
  `idCiudad` INT NOT NULL AUTO_INCREMENT,
  `idEstado` INT NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `poblacion` BIGINT NOT NULL,
  `territorioKm` double NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idCiudad`));

CREATE TABLE `aadhaar`.`estados` (
  `idEstado` INT NOT NULL AUTO_INCREMENT,
  `idRegion` INT NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `poblacion` BIGINT NOT NULL,
  `territorioKm` double NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idEstado`));

CREATE TABLE `aadhaar`.`regiones` (
  `idRegion` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `poblacion` BIGINT NOT NULL,
  `territorioKm` double NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idRegion`));

CREATE TABLE `aadhaar`.`estadoCivil` (
  `idEstadoCivil` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(100) NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idEstadoCivil`));

CREATE TABLE `aadhaar`.`generos` (
  `idGenero` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(100) NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idGenero`));

CREATE TABLE `aadhaar`.`idiomas` (
  `idIdioma` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(100) NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idIdioma`));

CREATE TABLE `aadhaar`.`inscripciones` (
  `idInscripcion` INT NOT NULL AUTO_INCREMENT,
  `idResidente` INT NOT NULL,
  `idUsuario` INT NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idInscripcion`));

CREATE TABLE `aadhaar`.`modulos` (
  `idModulo` INT NOT NULL AUTO_INCREMENT,
  `descripcion` TEXT NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idModulo`));

CREATE TABLE `aadhaar`.`permisos` (
  `idPermiso` INT NOT NULL AUTO_INCREMENT,
  `idUsuario` INT NOT NULL,
  `actualizar` TINYINT(1) NOT NULL,
  `eliminar` TINYINT(1) NOT NULL,
  `escribir` TINYINT(1) NOT NULL,
  `leer` TINYINT(1) NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idPermiso`));

CREATE TABLE `aadhaar`.`residentes` (
  `idResidente` INT NOT NULL AUTO_INCREMENT,
  `idGenero` INT NOT NULL,
  `idCiudad` INT NOT NULL,
  `idIdioma` INT NOT NULL,
  `idEstadoCivil` INT NOT NULL,
  `aadhaarId` INT(12) NOT NULL,
  `direccion` VARCHAR(200) NOT NULL,
  `fechaNacimiento` DATE NOT NULL,
  `nombre1` VARCHAR(50) NOT NULL,
  `nombre2` VARCHAR(50),
  `nombre3` VARCHAR(50),
  `apellido1` VARCHAR(50) NOT NULL,
  `apellido2` VARCHAR(50),
  `apellido3` VARCHAR(50),
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idResidente`));

CREATE TABLE `aadhaar`.`tipoBiometricos` (
  `idTipoBiometrico` INT NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(100) NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idTipoBiometrico`));

CREATE TABLE `aadhaar`.`usuarios` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `username` VARCHAR(20) NOT NULL,
  `password` VARCHAR(50) NOT NULL,
  `created_at` DATETIME NULL,
  `deleted_at` DATETIME NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`idUsuario`));

