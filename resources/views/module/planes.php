<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">{{moduletitle}}</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre del Plan</th>
					<th class="text-turquoise">Estado</th>
					<th class="text-turquoise">Fecha Inicio</th>
					<th class="text-turquoise">Fecha Fin</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="item in planes"
				ng-click="'planes/'+item.id | go">
					<td>{{item.nombre_plan}}</td>
					<td>{{item.estado_plan? "Activo" : "Inactivo"}}</td>
					<td>{{item.fecha_inicio_plan}}</td>
					<td>{{item.fecha_fin_plan}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/planes/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>