<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">{{moduletitle}}</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Usuario</th>
					<th class="text-turquoise">Módulo</th>
					<th class="text-turquoise">Actualizar</th>
					<th class="text-turquoise">Eliminar</th>
					<th class="text-turquoise">Escribir</th>
					<th class="text-turquoise">Leer</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="item in permisos"
				ng-click="'permisos/'+item.id | go">
					<td>{{item.usuario.username}}</td>
					<td>{{item.modulo.nombre}}</td>
					<td><span class="fui-checkbox-{{item.actualizar?'checked':'unchecked'}}"></span></td>
					<td><span class="fui-checkbox-{{item.eliminar?'checked':'unchecked'}}"></span></td>
					<td><span class="fui-checkbox-{{item.escribir?'checked':'unchecked'}}"></span></td>
					<td><span class="fui-checkbox-{{item.leer?'checked':'unchecked'}}"></span></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/permisos/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>