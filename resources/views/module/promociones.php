<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">{{moduletitle}}</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre de la Promoción</th>
					<th class="text-turquoise">Segmento</th>
					<th class="text-turquoise">Valor</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="item in promociones"
				ng-click="'promociones/'+item.id | go">
					<td>{{item.nombre_promocion}}</td>
					<td>{{item.segmentocliente.nombre_segmento}}</td>
					<td>Q. {{item.valor_promocion}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/promociones/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>