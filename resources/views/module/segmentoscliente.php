<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">{{moduletitle}}</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre del Segmento</th>
					<th class="text-turquoise">Meses</th>
					<th class="text-turquoise">Consumo Promedio Inicio</th>
					<th class="text-turquoise">Consumo Promedio Fin</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="item in segmentoscliente"
				ng-click="'segmentoscliente/'+item.id | go">
					<td>{{item.nombre_segmento}}</td>
					<td>{{item.meses_segmento}}</td>
					<td>Q. {{item.consumo_promedio_ini}}</td>
					<td>Q. {{item.consumo_promedio_fin}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/segmentoscliente/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>