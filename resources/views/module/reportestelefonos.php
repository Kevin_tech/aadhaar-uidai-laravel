<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Reporte de Teléfonos</h4>
		<span>Puedes utilizar los filtros para obtener mejores resultados</span>
	</div>
</div>

<form enctype="multipart/form-data" ng-submit="generateReportTelefonos()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<span>Marca</span>
			<select ng-model="reportetelefono.id_marca" style="display: block;">
				<option ng-repeat="item in marcas" value="{{item.id_marca_telefono}}">
					{{item.nombre_marca}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<span>Precio Min.</span>
			<input type="number" class="form-control" ng-model="reportetelefono.precio_min" placeholder="" />
		</div>
		<div class="col-xs-3">
			<span>Precio Max.</span>
			<input type="number" class="form-control" ng-model="reportetelefono.precio_max" placeholder="" />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Filtrar" name="enviar" />
		</div>
	</div>
</form>

<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre del Terminal</th>
					<th class="text-turquoise">Modelo</th>
					<th class="text-turquoise">Marca</th>
					<th class="text-turquoise">Costo</th>
					<th class="text-turquoise">Precio</th>
					<th class="text-turquoise">Subsidio</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="item in telefonos" 
				ng-click="'telefonos/'+item.id | go">
					<td>{{item.terminal_telefono}}</td>
					<td>{{item.modelo_telefono}}</td>
					<td>{{item.marcatelefono.nombre_marca}}</td>
					<td>Q. {{item.costo_telefono}}</td>
					<td>Q. {{item.precio_telefono}}</td>
					<td>Q. {{item.subsidio_telefono}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>