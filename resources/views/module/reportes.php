<div class="col_1_3">
	<h2>Listado de Expedientes</h2>

	<section ng-include="'form/report.expedientes'"></section>
</div>

<div class="col_1_3">
	<h2>Consolidado de Operaciones</h2>

	<section ng-include="'form/report.operaciones'"></section>
</div>

<div class="col_1_3">
	<h2>Tracking de Documento</h2>

	<section ng-include="'form/report.documento'"></section>
</div>

<div class="col_1_3" ng-if="me.admin==1">
	<h2>Validar con Firma Electrónica</h2>

	<section ng-include="'form/report.firmaelectronica'"></section>
</div>