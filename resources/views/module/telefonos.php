<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">{{moduletitle}}</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre del Terminal</th>
					<th class="text-turquoise">Modelo</th>
					<th class="text-turquoise">Marca</th>
					<th class="text-turquoise">Costo</th>
					<th class="text-turquoise">Precio</th>
					<th class="text-turquoise">Subsidio</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="item in telefonos"
				ng-click="'telefonos/'+item.id | go">
					<td>{{item.terminal_telefono}}</td>
					<td>{{item.modelo_telefono}}</td>
					<td>{{item.marcatelefono.nombre_marca}}</td>
					<td>Q. {{item.costo_telefono}}</td>
					<td>Q. {{item.precio_telefono}}</td>
					<td>Q. {{item.subsidio_telefono}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/telefonos/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>