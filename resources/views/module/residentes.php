<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">{{moduletitle}}</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Aadhaar ID</th>
					<th class="text-turquoise">Nombre del Residente</th>
					<th class="text-turquoise">Género</th>
					<th class="text-turquoise">Fecha de Nacimiento</th>
					<th class="text-turquoise">Estado Civil</th>
					<th class="text-turquoise">Dirección</th>
					<th class="text-turquoise">Ciudad</th>
					<th class="text-turquoise">Idioma</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="item in residentes"
				ng-click="'residentes/'+item.id | go">
					<td>{{item.aadhaarId}}</td>
					<td>{{item.nombre1}} {{item.nombre2}} {{item.nombre3}}
					{{item.apellido1}} {{item.apellido2}} {{item.apellido3}}</td>
					<td>{{item.genero.descripcion}}</td>
					<td>{{item.fechaNacimiento}}</td>
					<td>{{item.estadocivil.descripcion}}</td>
					<td>{{item.direccion}}</td>
					<td>{{item.ciudad.nombre}}</td>
					<td>{{item.idioma.descripcion}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/residentes/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>