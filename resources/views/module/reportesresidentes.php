<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Reporte de Residentes</h4>
		<span>Puedes utilizar los filtros para obtener mejores resultados</span>
	</div>
</div>

<form enctype="multipart/form-data" ng-submit="generateReportResidentes()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<span>Region</span>
			<select ng-model="reporteresidente.idRegion" style="display:block;">
				<option ng-repeat="item in regiones" value="{{item.idRegion}}">
					{{item.nombre}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<span>Estado</span>
			<select ng-model="reporteresidente.idEstado" style="display:block;">
				<option ng-repeat="item in estados | filter : { idRegion: reporteresidente.idRegion }" value="{{item.idEstado}}">
					{{item.nombre}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<span>Ciudad</span>
			<select ng-model="reporteresidente.idCiudad" style="display:block;">
				<option ng-repeat="item in ciudades | filter : { idEstado: reporteresidente.idEstado }" value="{{item.idCiudad}}">
					{{item.nombre}}
				</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Filtrar" name="enviar" />
		</div>
	</div>
</form>

<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre del Residente</th>
					<th class="text-turquoise">No. Aadhaar</th>
					<th class="text-turquoise">Ciudad</th>
					<th class="text-turquoise">Dirección</th>
					<th class="text-turquoise">Género</th>
					<th class="text-turquoise">Estado Civil</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="item in residentes" 
				ng-click="'residentes/'+item.id | go">
					<td>{{item.nombre1}} {{item.nombre2}} {{item.nombre3}}
					{{item.apellido1}} {{item.apellido2}} {{item.apellido3}}</td>
					<td>{{item.aadhaarId}}</td>
					<td>{{item.ciudad.nombre}}</td>
					<td>{{item.direccion}}</td>
					<td>{{item.genero.descripcion}}</td>
					<td>{{item.estadocivil.descripcion}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>