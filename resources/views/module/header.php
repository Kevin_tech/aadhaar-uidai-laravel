<div class="row demo-row">
  <div class="col-xs-2 col-xs-offset-1">
    <img src="img/aadhaar-logo.png" width="60%">
  </div>
  <div class="col-xs-8">
    <nav class="navbar navbar-inverse navbar-embossed" role="navigation">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01">
          <span class="sr-only">Toggle navigation</span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse-01">
        <ul class="nav navbar-nav navbar-left">
          <li><a href="#/home">Inicio</a></li>
          <li class="dropdown">
            <a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown">Residentes <b class="caret"></b></a>
            <span class="dropdown-arrow"></span>
            <ul class="dropdown-menu">
              <li><a href="#/residentes/new">Inscripción</a></li>
              <li><a href="#/residentes">Residentes</a></li>
              <li class="divider"></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown">Configuración <b class="caret"></b></a>
            <span class="dropdown-arrow"></span>
            <ul class="dropdown-menu">
              <li><a href="#/idiomas">Idiomas</a></li>
              <li><a href="#/usuarios">Usuarios</a></li>
              <li class="divider"></li>
              <li><a href="#/modulos">Módulos</a></li>
              <li><a href="#/permisos">Permisos</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="javascript:void;" class="dropdown-toggle" data-toggle="dropdown">Reportes <b class="caret"></b></a>
            <span class="dropdown-arrow"></span>
            <ul class="dropdown-menu">
              <li><a href="#/reportes/residentes">Residentes</a></li>
              <li><a href="#/reportes/telefonos">Inscripciones</a></li>
              <li class="divider"></li>
            </ul>
          </li>
          <li><a href="#/logout">Salir</a></li>
        </ul>
      </div>
    </nav>
  </div>
</div>