<div class="row">
	<div class="col-xs-10 col-xs-offset-1">
		<h4 class="title-section">{{moduletitle}}</h4>
	</div>

	<div class="col-xs-10 col-xs-offset-1">
		<table width="100%">
			<thead>
				<tr>
					<th class="text-turquoise">Nombre</th>
					<th class="text-turquoise">Username</th>
					<th class="text-turquoise">E-Mail</th>
				</tr>
			</thead>

			<tbody>
				<tr ng-repeat="item in usuarios"
				ng-click="'usuarios/'+item.id | go">
					<td>{{item.nombre}}</td>
					<td>{{item.username}}</td>
					<td>{{item.email}}</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-xs-2 col-xs-offset-1 keypad">
		<a href="#/usuarios/new" class="btn btn-block btn-lg btn-info">
			<span class="fui-plus"></span>
			Nuevo Registro
		</a>
	</div>
</div>