<section class="col_full content-article">
	<h2>
		{{pagocheque.apellido_1}} {{pagocheque.apellido_2}} {{pagocheque.apellido_3}},
		{{pagocheque.nombre_1}} {{pagocheque.nombre_2}} {{pagocheque.nombre_3}}
	</h2>

	<article>
		<img ng-if="pagocheque.src_foto.length>0" class="image" src="{{pagocheque.src_foto}}">
		<label class="seq">
			DPI: {{pagocheque.numero_dpi}}
			<button class="print" ng-click="generateReportPagoCheque()">
				<span class="icon-print"></span>
				Obtener Reporte
			</button>
		</label>
		<p>
			<b>Renglón</b><br/>
			{{pagocheque.renglon.nombre}}
		</p>
		<p>
			<b>Unidad</b><br/>
			{{pagocheque.unidad.nombre}}
		</p>
		<p>
			<b>Puesto que ocupa</b><br/>
			{{pagocheque.puesto}}
		</p>
		<div class="clear"></div>
		<div class="details">
			<label class="date">
				<span class="icon-today"></span>
				{{pagocheque.created_at | amDateFormat:'dddd D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</label>
		</div>
	</article>

	<div class="messages content-block">
		<!-- Asignaciones area -->
		<div class="col_full">
			<h3>Datos sobre el pago</h3>
			<p>
				<b>Número de referencia del cheque</b><br/>
				{{pagocheque.numero_cheque}}
			</p>
			<p>
				<b>Monto del cheque</b><br/>
				Q. {{pagocheque.monto}}
			</p>
			<p ng-if="pagocheque.src_cheque.length>0">
				<b>Foto del cheque</b><br/>
				<img class="image" src="{{pagocheque.src_cheque}}">
			</p>
		</div>
	</div>
</section>