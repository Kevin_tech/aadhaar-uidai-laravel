<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>Ingresa los datos para la nueva oferta laboral</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="empleo.nombreempleo" placeholder="Nombre del empleo" required />
		</div>
		<div class="col-xs-6">
			<input type="text" class="form-control" ng-model="empleo.descripcionempleo" placeholder="Descripción" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="empleo.requerimientos" placeholder="Requerimientos" required />
		</div>
		<div class="col-xs-3">
			<input type="date" class="form-control" ng-model="empleo.fechacontratacion" placeholder="Fecha de contratación" required />
		</div>
		<div class="col-xs-3">
			<input type="date" class="form-control" ng-model="empleo.fechapostulacion" placeholder="Fecha de postulación" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="number" class="form-control" ng-model="empleo.sueldo" placeholder="Sueldo en Quetzales" required />
		</div>
		<div class="col-xs-3">
			<select ng-model="empleo.iddepartamento">
				<option ng-repeat="departamento in departamentos" value="{{departamento.id}}">
					{{departamento.descripciondepartamento}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<select ng-model="empleo.idmunicipio">
				<option ng-repeat="municipio in municipios | filter : { iddepartamento: empleo.iddepartamento }" value="{{municipio.id}}">
				{{municipio.descripcionmunicipio}}
			</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="number" min="1" class="form-control" ng-model="empleo.numerovacantes" placeholder="Número de Vacantes" required />
		</div>
		<div class="col-xs-3">
			<select ng-model="empleo.idjornada">
				<option ng-repeat="jornada in jornadas" value="{{jornada.id}}">
					{{jornada.nombrejornada}}
				</option>
			</select>
		</div>
		<div class="col-xs-3">
			<select ng-model="empleo.idempresa">
				<option ng-repeat="empresa in empresas" value="{{empresa.id}}">
					{{empresa.nombreempresa}}
				</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Modificar" name="enviar" />
		</div>
	</div>
</form>