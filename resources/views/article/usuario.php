<section class="wrapper_center">
	<section class="col_2_3 profile">
		<h2>
			{{usuario.apellido_1}} {{usuario.apellido_2}},
			{{usuario.nombre_1}} {{usuario.nombre_2}}
		</h2>
		<ul class="details">
			<li>
				<span class="icon-person"></span>
				<strong>Nombre de usuario</strong>
				{{usuario.usuario}}
			</li>
			<li>
				<span class="icon-work"></span>
				<strong>{{usuario.puesto.nombre}}</strong>
				en <strong>{{usuario.unidad.nombre}}</strong>
			</li>
			<li ng-if="usuario.admin==1">
				<span class="icon-security"></span>
				Eres administrador del sistema
			</li>
		</ul>
		<ul class="resume">
			<li>
				<span class="icon-library_add"></span>
				<strong>{{usuario.documentocreado.length}}</strong>
			</li>
			<li>
				<span class="icon-person_add"></span>
				<strong>{{usuario.documentoasigno.length}}</strong>
			</li>
			<li>
				<span class="icon-done_all"></span>
				<strong>{{usuario.documentoasignado.length}}</strong>
			</li>
		</ul>

		<article>
			<form class="form-block" ng-if="usuario.you" ng-submit="changePassword()">
				<div class="field-block">
					<label>Contraseña actual</label>
					<input type="password" ng-model="usuario.oldpassword" maxlength="50" required />
				</div>
				<div class="field-block">
					<label>Nueva contraseña</label>
					<input type="password" ng-model="usuario.password" maxlength="50" required />
				</div>
				<div class="field-block">
					<label>Repite tu nueva contraseña</label>
					<input type="password" ng-model="usuario.password_confirmation" maxlength="50" required />
				</div>
				<input type="submit" value="Enviar" name="enviar" />
				<div class="keypad">
					<button class="submit">Cambiar mi contraseña</button>
				</div>
			</form>

			
		</article>
	</section>
</section>