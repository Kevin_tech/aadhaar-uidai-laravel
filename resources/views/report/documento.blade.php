<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">

		html,body
		{
			font-family: Arial;
			font-size: 14px;
			padding-top: 60px;
		}

		table
		{
			border-collapse: collapse;
		}

		.history
		{
			border: 0;
			margin: 1em 0;
		}

		.history td
		{
			padding: 0.25em 0.5em;
		}

		.history thead tr td
		{
			background: #CEECFA;
			border-bottom: 1px solid black;
			border-right: 1px solid black;
			font-weight: bold;
		}

		.history thead tr td:last-of-type
		{
			border-right: none;
		}

		.history .column
		{
			background: #CEECFA;
			border-bottom: 1px solid black;
			border-right: 1px solid black;
			font-weight: bold;
		}

		.history .column:last-of-type
		{
			border-bottom: 0;
		}

		.history .corner
		{
			background: none;
		}

		.footer
		{
			bottom: 0;
			height: 42px;
			position: fixed;
			right: 0;
			text-align: center;
			width: 100%;
		}

		.footer img
		{
			height: 100%;
		}

		.footer .digitalsignature
		{
			font-size: 12px;
		}

		.logo
		{
			height: 36px;
			left: 0;
			position: fixed;
			top: 0;
		}

		.logo img
		{
			height: 100%;
		}

		.page-break
		{
			page-break-after: always;
		}

		.referencia
		{
			margin: 0 auto 1.5em;
			width: 375px;
		}

		.reply
		{
			padding: 1em 0;
			border-bottom: 1px solid #AAA;
		}

		.reply .msg
		{
			margin-top: 1em;
		}

		.reply .status
		{
			display: block;
		}

		.reply .user
		{
			display: block;
			font-weight: bold;
		}

		.resumen
		{
			border: 1px solid black;
			font-size: 11px;
			margin-bottom: 1em;
			padding: 1em;
			position: absolute;
			right: 0;
			top: 0;
			width: 220px;
		}

		.resumen h1
		{
			background: white;
			font-size: 12px;
			margin: -1.5em auto 0;
			padding: 0;
			text-align: center;
			width: 110px;
		}

		.signatures
		{
			width: 100%;
			position: relative;
		}

		.signatures > div
		{
			border-top: 1px solid black;
			font-weight: bold;
			text-align: center;
			margin-top: 100px;
			position: absolute;
			width: 220px;
		}

		.signatures .center
		{
			left: 50%;
			margin-left: -150px;
			top: 100px;
			width: 300px;
		}

		.signatures .left
		{
			left: 1em;
		}

		.signatures .right
		{
			right: 1em;
		}
	</style>
</head>
<body>

	<?php
		$asignacion_current = null;
		foreach ($data->asignacion as $keyAsignacion => $valueAsignacion)
		{
			if( $valueAsignacion->activo )
				$asignacion_current = $valueAsignacion;
		}
	?>

	<div class="logo">
		<img src="{{public_path()}}/img/infom.png" />
	</div>

	<div class="footer">
		<img src="{{public_path()}}/img/it.png" />
		<div class="digitalsignature">
			<b>Firma digital:</b>
			{{$digital_signature}}
		</div>
	</div>

	<div class="resumen">
		<h1>EXPEDIENTE</h1>
		<span>{{$data->correlativo}}</span><br/>
		<label><b>Fecha / Hora:</b> {{date("d/m/Y H:i:s")}}</label>
	</div>

	<table class="referencia">
		<tr>
			<td><b>No. Documento de Referencia:</b></td>
			<td>{{$data->documento_referencia}}</td>
		</tr>
		<tr>
			<td><b>Fecha del Documento de Referencia:</b></td>
			<td>{{$data->fecha_referencia}}</td>
		</tr>
	</table>

	<table class="cabecera">
		<tr>
			<td><b>De:</b></td>
			<td>{{$data->usuario_creo->apellido_1}} {{$data->usuario_creo->apellido_2}}, {{$data->usuario_creo->nombre_1}} {{$data->usuario_creo->nombre_2}}</td>
		</tr>
		<tr>
			<td><b>Para:</b></td>
			<td>
				@if( $asignacion_current )
					{{$asignacion_current->usuario_asignado->unidad->nombre}}
				@endif
			</td>
		</tr>
		<tr>
			<td><b>Asunto:</b></td>
			<td>{{$data->asunto}}</td>
		</tr>
		<tr>
			<td><b>Estado:</b></td>
			<td>{{$data->estado->nombre}}</td>
		</tr>
		<tr>
			<td><b>Fecha de envío:</b></td>
			<td>{{date("d/m/Y H:i:s", strtotime($data->created_at))}}</td>
		</tr>
		<tr>
			<td><b>Días para responder:</b></td>
			<td>{{$data->dias_respuesta}} día(s)</td>
		</tr>
		<tr>
			<td><b>Observaciones:</b></td>
			<td></td>
		</tr>
	</table>

	@foreach( $data->asignacion as $asignacion )
		@if( $asignacion->id_estado == 2 )
			<div class="reply">
				<span class="status">[ TRASLADADO @if( $asignacion->compartido ) COMPARTIDO @endif ]</span>
				<span class="user">
					{{$asignacion->usuario_asignado->apellido_1}} {{$asignacion->usuario_asignado->apellido_2}},
					{{$asignacion->usuario_asignado->nombre_1}} {{$asignacion->usuario_asignado->nombre_2}}
					- {{date("d/m/Y H:i:s", strtotime($asignacion->created_at))}}
				</span>
				<span>
					<b>Trasladado por:</b>
					{{$asignacion->usuario_asigno->apellido_1}} {{$asignacion->usuario_asigno->apellido_2}},
					{{$asignacion->usuario_asigno->nombre_1}} {{$asignacion->usuario_asigno->nombre_2}}
				</span>
				<span>
					<b>Mensaje de traslado:</b>
					{{$asignacion->mensaje_asignacion}}
				</span>
			</div>
		@endif
		@if( $asignacion->fecha_recibido != "0000-00-00 00:00:00" )
			<div class="reply">
				<span class="status">[ RECIBIDO ]</span>
				<span class="user">
					{{$asignacion->usuario_asignado->apellido_1}} {{$asignacion->usuario_asignado->apellido_2}},
					{{$asignacion->usuario_asignado->nombre_1}} {{$asignacion->usuario_asignado->nombre_2}}
					- {{date("d/m/Y H:i:s", strtotime($asignacion->fecha_recibido))}}
				</span>
				<div class="msg">
					{{$asignacion->mensaje_recibido}}
				</div>
			</div>
		@elseif( $asignacion->id_estado != 2 )
			<div class="reply">
				<span class="status">[ ASIGNACION ]</span>
				<span class="user">
					{{$asignacion->usuario_asignado->apellido_1}} {{$asignacion->usuario_asignado->apellido_2}},
					{{$asignacion->usuario_asignado->nombre_1}} {{$asignacion->usuario_asignado->nombre_2}}
					- {{date("d/m/Y H:i:s", strtotime($asignacion->fecha_recibido))}}
				</span>
				<div class="msg">
					{{$asignacion->mensaje_recibido}}
				</div>
			</div>
		@endif
		@if( $asignacion->fecha_cierre != "0000-00-00 00:00:00" )
			<div class="reply">
				<span class="status">[ FINALIZACIÓN ]</span>
				<span class="user">
					{{$asignacion->usuario_asignado->apellido_1}} {{$asignacion->usuario_asignado->apellido_2}},
					{{$asignacion->usuario_asignado->nombre_1}} {{$asignacion->usuario_asignado->nombre_2}}
					- {{date("d/m/Y H:i:s", strtotime($asignacion->fecha_recibido))}}
				</span>
				<div class="msg">
					{{$asignacion->mensaje_cierre}}
				</div>
			</div>
		@endif
	@endforeach

	<div class="page-break"></div>

	<div class="resumen">
		<h1>EXPEDIENTE</h1>
		<span>{{$data->correlativo}}</span><br/>
		<label><b>Fecha / Hora:</b> {{date("d/m/Y H:i:s")}}</label>
	</div>

	<table class="referencia">
		<tr>
			<td><b>No. Documento de Referencia:</b></td>
			<td>{{$data->documento_referencia}}</td>
		</tr>
		<tr>
			<td><b>Fecha del Documento de Referencia:</b></td>
			<td>{{$data->fecha_referencia}}</td>
		</tr>
	</table>

	<table class="cabecera">
		<tr>
			<td><b>De:</b></td>
			<td>{{$data->usuario_creo->unidad->nombre}}</td>
		</tr>
		<tr>
			<td><b>Para:</b></td>
			<td>
				@if( $asignacion_current )
					{{$asignacion_current->usuario_asignado->unidad->nombre}}
				@endif
			</td>
		</tr>
		<tr>
			<td><b>Asunto:</b></td>
			<td>{{$data->asunto}}</td>
		</tr>
	</table>

	<table class="history" >
		<thead>
			<tr>
				<td class="corner"></td>
				<td>INGRESO</td> 
				<td>TRASLADO</td>
				<td>RECEPCION</td>
				<td>ANULACION</td>
				<td>FINALIZACION</td>
				<td>SUSPENSION</td>
				<td>RECHAZO</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="column">Usuario:</td>
				<td>{{$data->usuario_creo->usuario}}</td>
				<td>
					@foreach($data->asignacion as $asignacion)
						@if($asignacion->id_estado==2)
							{{$asignacion->usuario_asigno->usuario}}
						@endif
					@endforeach
				</td>
				<td>
					@foreach($data->asignacion as $asignacion)
						@if($asignacion->activo && $asignacion->fecha_recibido != "0000-00-00 00:00:00")
							{{$asignacion->usuario_asignado->usuario}}
						@endif
					@endforeach
				</td>
				<td></td>
				<td>
					@foreach($data->asignacion as $asignacion)
						@if($asignacion->activo && $asignacion->fecha_cierre != "0000-00-00 00:00:00")
							{{$asignacion->usuario_asignado->usuario}}
						@endif
					@endforeach
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="column">Fecha:</td>
				<td>{{date("d/m/Y", strtotime($data->created_at))}}</td>
				<td>
					@foreach($data->asignacion as $asignacion)
						@if($asignacion->id_estado==2 && $asignacion->activo)
							{{date("d/m/Y", strtotime($asignacion->created_at))}}
						@endif
					@endforeach
				</td>
				<td>
					@foreach($data->asignacion as $asignacion)
						@if($asignacion->activo && $asignacion->fecha_recibido != "0000-00-00 00:00:00")
							{{date("d/m/Y", strtotime($asignacion->fecha_recibido))}}
						@endif
					@endforeach
				</td>
				<td></td>
				<td>
					@foreach($data->asignacion as $asignacion)
						@if($asignacion->activo && $asignacion->fecha_cierre != "0000-00-00 00:00:00")
							{{date("d/m/Y", strtotime($asignacion->fecha_cierre))}}
						@endif
					@endforeach
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="column">Hora:</td>
				<td>{{date("H:i:s", strtotime($data->created_at))}}</td>
				<td>
					@foreach($data->asignacion as $asignacion)
						@if($asignacion->id_estado==2 && $asignacion->activo)
							{{date("H:i:s", strtotime($asignacion->created_at))}}
						@endif
					@endforeach
				</td>
				<td>
					@foreach($data->asignacion as $asignacion)
						@if($asignacion->activo && $asignacion->fecha_recibido != "0000-00-00 00:00:00")
							{{date("H:i:s", strtotime($asignacion->fecha_recibido))}}
						@endif
					@endforeach
				</td>
				<td></td>
				<td>
					@foreach($data->asignacion as $asignacion)
						@if($asignacion->activo && $asignacion->fecha_cierre != "0000-00-00 00:00:00")
							{{date("H:i:s", strtotime($asignacion->fecha_cierre))}}
						@endif
					@endforeach
				</td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	</table>

	<div class="signatures">
		<div class="left">
			Firma y Nombre<br/>
			{{$data->usuario_creo->nombre_1}} {{$data->usuario_creo->nombre_2}} 
			{{$data->usuario_creo->apellido_1}} {{$data->usuario_creo->apellido_2}} 
		</div>

		<div class="center">
			Firma y Nombre<br/>
			@if( $asignacion_current )
				{{$asignacion_current->usuario_asignado->nombre_1}} {{$asignacion_current->usuario_asignado->nombre_2}} 
				{{$asignacion_current->usuario_asignado->apellido_1}} {{$asignacion_current->usuario_asignado->apellido_2}} 
			@endif
		</div>

		<div class="right">
			Firma y Nombre<br/>
			@if( $asignacion_current )
				{{$asignacion_current->usuario_asignado->unidad->director->nombre_1}} {{$asignacion_current->usuario_asignado->unidad->director->nombre_2}}
				{{$asignacion_current->usuario_asignado->unidad->director->apellido_1}} {{$asignacion_current->usuario_asignado->unidad->director->apellido_2}}
			@endif
		</div>
	</div>

</body>
</html>