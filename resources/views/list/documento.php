<section class="content-block">
	<header>
		<h2>Bandeja de entrada</h2>
		<div class="filter">
			<form class="form-inline">
				<div class="input-search">
					<input type="text" ng-model="documentoFiltro.$" placeholder="Ingresa tu búsqueda aquí" />
					<button class="submit">
						Buscar
						<span class="icon-search"></span>
					</button>
				</div>
				<div class="input-options">
					<div class="select">
						Filtrar por estado:
						<select ng-model="documentoFiltro.id_estado">
							<option value="">--- TODOS ---</option>
							<option ng-repeat="estado in estados" value="{{estado.id}}">
								{{estado.nombre}}
							</option>
						</select>
						<label ng-show="documentoFiltro.id_estado==1">
							<input type="checkbox" ng-model="documentoLeido" value="1" >
							Mostrar Leídos
						</label>
					</div>
				</div>
			</form>
		</div>
	</header>

	<div class="listview">
		<div class="item add-item">
			<button ng-click="'documentos/new' | go">
				<span class="icon-add_circle"></span>
				Agregar un nuevo documento
			</button>
		</div>
		<div class="item" ng-repeat="documento in documentos | filter:documentoFiltro | startFrom: pagination.page * pagination.perPage | limitTo: pagination.perPage" 
			ng-if="documento.id_estado != 1 || ( documento.id_estado == 1 && documento.fecha_recibido == '0000-00-00 00:00:00' ) || ( documento.id_estado == 1 && documento.fecha_recibido != '0000-00-00 00:00:00' && documentoLeido )"
			ng-click="'documentos/'+documento.id | go">
			<label class="priority-tag days square left" 
					ng-class="{ ontime: documento.dias_diferencia_respuesta>2, inhurry: documento.dias_diferencia_respuesta>0, today: documento.dias_diferencia_respuesta==0, outtime: documento.dias_diferencia_respuesta<0 }"
					ng-if="documento.id_estado < 3">
				<span>
					{{ ( documento.dias_diferencia_respuesta = documento.dias_respuesta - ( current_date | amDifference : documento.created_at : 'days' ) ) | abs }}
				</span>
			</label>
			<label class="subject">
				<span ng-class="{ 'icon-attach_file': documento.src_file.length > 0 }"></span>
				{{documento.asunto}}
				<span class="received" ng-if="documento.recibido>0 && documento.visto==0"></span>
				<span class="viewed" ng-if="documento.visto>0"></span>
			</label>
			<div>
				<span class="icon-person"></span>
				<strong>Creado por:</strong>
				{{documento.usuario_creo.apellido_1}} {{documento.usuario_creo.apellido_2}}, {{documento.usuario_creo.nombre_1}} {{documento.usuario_creo.nombre_2}}
				<br/>
				<span class="icon-today"></span>
				{{documento.created_at | amDateFormat:'D [de] MMMM [del] YYYY, [a las] hh:mm a'}}
			</div>
		</div>
		<div class="pagination" ng-if="documentos.length > pagination.perPage">
			<label class="prev">
				<a href="" ng-click="pagination.prevPage()">
					<span class="icon-navigate_before"></span>
				</a>
			</label>
			<ul class="numbers">
				<li ng-repeat="n in [] | range: pagination.numPages" ng-class="{active: n == pagination.page}">
					<a href="" ng-click="pagination.toPageId(n)">{{n + 1}}</a>
				</li>
			</ul>
			<label class="next">
				<a href="" ng-click="pagination.nextPage()">
					<span class="icon-navigate_next"></span>
				</a>
			</label>
		</div>
		<div class="item not-found" ng-if="documentos.length == 0">
			No se encontraron resultados...
		</div>
	</div>
</section>