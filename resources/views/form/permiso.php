<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>{{moduletitle}}</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			Seleccione el Módulo: <br/>
			<select ng-model="permiso.idModulo" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="modulo in modulos" value="{{modulo.idModulo}}">
					{{modulo.nombre}}
				</option>
			</select>
		</div>
		<div class="col-xs-6">
			Usuario a asignar: <br/>
			<select ng-model="permiso.idUsuario" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="usuario in usuarios" value="{{usuario.id}}">
					{{usuario.username}}
				</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			Seleccione los permisos que desea otorgar: <br/>
			<label>
				<input type="checkbox" ng-model="permiso.actualizar">
				Actualizar
			</label><br/>
			<label>
				<input type="checkbox" ng-model="permiso.eliminar">
				Eliminar
			</label><br/>
			<label>
				<input type="checkbox" ng-model="permiso.escribir">
				Escribir
			</label><br/>
			<label>
				<input type="checkbox" ng-model="permiso.leer">
				Leer
			</label>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>