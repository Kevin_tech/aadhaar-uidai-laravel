<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>{{moduletitle}}</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="number" class="form-control" ng-model="numerotelefono.numero_telefono" placeholder="Número de Teléfono" required />
		</div>
		<div class="col-xs-6">
			<select ng-model="numerotelefono.id_cliente" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="cliente in clientes" value="{{cliente.id_cliente}}">
					{{cliente.nombre_cliente}}
				</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<select ng-model="numerotelefono.estado_numero_telefono" class="form-control select select-primary" data-toggle="select">
				<option value="0">Inactivo</option>
				<option value="1">Activo</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>