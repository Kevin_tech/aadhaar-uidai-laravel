<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>{{moduletitle}}</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="telefono.terminal_telefono" placeholder="Terminal" required />
		</div>
		<div class="col-xs-6">
			<input type="text" class="form-control" ng-model="telefono.modelo_telefono" placeholder="Modelo" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<select ng-model="telefono.id_marca" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="marca in marcas" value="{{marca.id_marca_telefono}}">
					{{marca.nombre_marca}}
				</option>
			</select>
		</div>
		<div class="col-xs-6">
			<input type="number" class="form-control" ng-model="telefono.costo_telefono" placeholder="Costo" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="number" class="form-control" ng-model="telefono.precio_telefono" placeholder="Precio" required />
		</div>
		<div class="col-xs-6">
			<input type="number" class="form-control" ng-model="telefono.subsidio_telefono" placeholder="Subsidio" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>