<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>{{moduletitle}}</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="segmentocliente.nombre_segmento" placeholder="Nombre del Segmento" required />
		</div>
		<div class="col-xs-6">
			<input type="number" class="form-control" ng-model="segmentocliente.meses_segmento" placeholder="Cantidad de Meses" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="number" class="form-control" ng-model="segmentocliente.consumo_promedio_ini" placeholder="Consumo Promedio Inicial" required />
		</div>
		<div class="col-xs-6">
			<input type="number" class="form-control" ng-model="segmentocliente.consumo_promedio_fin" placeholder="Consumo Promedio Final" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>