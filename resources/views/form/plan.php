<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>{{moduletitle}}</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="plan.nombre_plan" placeholder="Nombre del Plan" required />
		</div>
		<div class="col-xs-6">
			<select ng-model="plan.estado_plan" class="form-control select select-primary" data-toggle="select">
				<option value="0">Inactivo</option>
				<option value="1">Activo</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="date" class="form-control" ng-model="plan.fecha_inicio_plan" placeholder="Fecha de Inicio" required />
		</div>
		<div class="col-xs-6">
			<input type="date" class="form-control" ng-model="plan.fecha_fin_plan" placeholder="Fecha Fin" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>