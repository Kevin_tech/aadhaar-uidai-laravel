<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>{{moduletitle}}</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-2 col-xs-offset-1">
			Nombres
		</div>
		<div class="col-xs-2">
			<input type="text" class="form-control" ng-model="residente.nombre1" required />
		</div>
		<div class="col-xs-2">
			<input type="text" class="form-control" ng-model="residente.nombre2" required />
		</div>
		<div class="col-xs-2">
			<input type="text" class="form-control" ng-model="residente.nombre3" />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-2 col-xs-offset-1">
			Apellidos
		</div>
		<div class="col-xs-2">
			<input type="text" class="form-control" ng-model="residente.apellido1" required />
		</div>
		<div class="col-xs-2">
			<input type="text" class="form-control" ng-model="residente.apellido2" required />
		</div>
		<div class="col-xs-2">
			<input type="text" class="form-control" ng-model="residente.apellido3" placeholder="Apellido de Casada" />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-2 col-xs-offset-1">
			Fecha de Nacimiento
		</div>
		<div class="col-xs-4">
			<input type="date" class="form-control" ng-model="residente.fechaNacimiento" required />
		</div>
		<div class="col-xs-4 col-xs-offset-1">
			Idioma
			<select ng-model="residente.idIdioma" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="idioma in idiomas" value="{{idioma.idIdioma}}">
					{{idioma.descripcion}}
				</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-5 col-xs-offset-1">
			Region
			<select ng-model="residente.idRegion" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="region in regiones" value="{{region.idRegion}}">
					{{region.nombre}}
				</option>
			</select>
		</div>
		<div class="col-xs-5">
			Estado
			<select ng-model="residente.idEstado" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="estado in estados | filter : { idRegion: residente.idRegion }" value="{{estado.idEstado}}">
					{{estado.nombre}}
				</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-5 col-xs-offset-1">
			Ciudad
			<select ng-model="residente.idCiudad" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="ciudad in ciudades | filter : { idEstado: residente.idEstado }" value="{{ciudad.idCiudad}}">
					{{ciudad.nombre}}
				</option>
			</select>
		</div>
		<div class="col-xs-5">
			<input type="text" class="form-control" ng-model="residente.direccion" placeholder="Dirección" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-5 col-xs-offset-1">
			Genero
			<select ng-model="residente.idGenero" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="genero in generos" value="{{genero.idGenero}}">
					{{genero.descripcion}}
				</option>
			</select>
		</div>
		<div class="col-xs-5">
			Estado Civil
			<select ng-model="residente.idEstadoCivil" class="form-control select select-primary" data-toggle="select">
				<option ng-repeat="estado in estadoCivil" value="{{estado.idEstadoCivil}}">
					{{estado.descripcion}}
				</option>
			</select>
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>