<div class="row">
	<div class="col-xs-8 col-xs-offset-1">
		<h4>{{moduletitle}}</h4>
	</div>
</div>
<form enctype="multipart/form-data" ng-submit="submit()">
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="text" class="form-control" ng-model="factura.serie_factura" placeholder="Serie" required />
		</div>
		<div class="col-xs-6">
			<input type="number" class="form-control" ng-model="factura.numero_factura" placeholder="Número" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<select ng-model="factura.estado_factura" class="form-control select select-primary" data-toggle="select">
				<option value="0">Inactivo</option>
				<option value="1">Activo</option>
			</select>
		</div>
		<div class="col-xs-6">
			<input type="date" class="form-control" ng-model="factura.fecha_factura" placeholder="Fecha" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-4 col-xs-offset-1">
			<input type="number" class="form-control" ng-model="factura.total_factura" placeholder="Monto total" required />
		</div>
	</div>
	<div class="row row-gutter">
		<div class="col-xs-10 col-xs-offset-1">
			<input class="btn btn-lg btn-success" type="submit" value="Guardar" name="enviar" />
		</div>
	</div>
</form>