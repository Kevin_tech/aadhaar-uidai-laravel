<!DOCTYPE html>
<html ng-app="appTracking">
<head>
	<title>Bienvenido :: Aadhaar Unique Identification System</title>
	<meta charset="utf-8" />
	<meta name="author" content="Kevin Herrarte - (at)kevin_tech" />
	<meta name="description" content="CRM developed with love">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<link href="./css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="./css/flat-ui.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/favicon.ico">
	<link rel="stylesheet" type="text/css" href="./bower_components/sweetalert2/dist/sweetalert2.css">
	<link rel="stylesheet" type="text/css" href="./bower_components/angular-loading-bar/build/loading-bar.min.css">

	<script type="text/javascript" src="./bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
	<script type="text/javascript" src="./bower_components/angular/angular.min.js"></script>
	<script type="text/javascript" src="./bower_components/angular-resource/angular-resource.min.js"></script>
	<script type="text/javascript" src="./bower_components/ngstorage/ngStorage.min.js"></script>
	<script type="text/javascript" src="./bower_components/angular-loading-bar/build/loading-bar.min.js"></script>
	<script type="text/javascript" src="./bower_components/ngSweetAlert/SweetAlert.min.js"></script>
	<script type="text/javascript" src="./js/login.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="./js/vendor/html5shiv.js"></script>
      <script src="./js/vendor/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
    	body { 
    		padding-top: 150px;
    	}

    	.login-form {
    		background-color: #34495E;
    	}
    </style>
</head>

<body ng-controller="LoginController">

	<div class="row">
		<div class="col-xs-4 col-xs-offset-1">
			<img src="img/aadhaar-logo.png" alt="Welcome to Aadhaar Unique Identification System" />
		</div>

		<div class="login-form col-xs-5 col-xs-offset-1">
			<form method="post" ng-submit="submit()" ng-if="loginform">
				<div class="form-group">
					<input type="text" class="form-control login-field" value="" placeholder="Ingresa tu usuario" id="login-name" ng-model="credencial.username"/>
					<label class="login-field-icon fui-user" for="login-name"></label>
				</div>

				<div class="form-group">
					<input type="password" class="form-control login-field" value="" placeholder="Contraseña" id="login-pass" ng-model="credencial.password"/>
					<label class="login-field-icon fui-lock" for="login-pass"></label>
				</div>

				<a class="btn btn-primary btn-lg btn-block" ng-click="submit()" href="#">Iniciar Sesión</a>
			</form>

			<form method="post" ng-submit="register()" ng-if="!loginform">
				<div class="form-group">
					<input type="text" class="form-control login-field" value="" placeholder="Ingresa tu usuario" id="register-name" ng-model="usuarioRegistro.username"/>
					<label class="login-field-icon fui-user" for="register-name"></label>
				</div>

				<div class="form-group">
					<input type="password" class="form-control login-field" value="" placeholder="Contraseña" id="register-pass" ng-model="usuarioRegistro.password"/>
					<label class="login-field-icon fui-lock" for="register-pass"></label>
				</div>

				<a class="btn btn-primary btn-lg btn-block" ng-click="register()" href="#">Registrarme</a>
			</form>
			
			<div>
				<a class="login-link" href="#">¿Olvidaste tu contraseña?</a>
				<a class="login-link" href="#" ng-click="loginform=false" ng-show="loginform">¿Aún no eres miembro?</a>
				<a class="login-link" href="#" ng-click="loginform=true" ng-show="!loginform">Ya tengo una cuenta</a>
			</div>
		</div>
	</div>

</body>
</html>
