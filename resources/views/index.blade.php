<!DOCTYPE html>
<html ng-app="appTracking">
<head>
	<title ng-bind="title">Inicio :: Aadhaar Unique Identification System</title>
	<meta charset="utf-8" />
	<meta name="author" content="Kevin Herrarte - (at)kevin_tech" />
	<meta name="description" content="Unique Identification System">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<link href="./css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="./css/flat-ui.css" rel="stylesheet">
	<link rel="shortcut icon" href="img/favicon.ico">
	<link rel="stylesheet" type="text/css" href="./bower_components/sweetalert2/dist/sweetalert2.css">
	<link rel="stylesheet" type="text/css" href="./bower_components/angular-loading-bar/build/loading-bar.min.css">
	<link href="./css/all.css" rel="stylesheet">

	<script src="./js/vendor/jquery.min.js"></script>
	<script src="./js/flat-ui.min.js"></script>
	<script type="text/javascript" src="./js/all.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="./js/vendor/html5shiv.js"></script>
      <script src="./js/vendor/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	
	<header ng-include="'module/header'" ng-controller="HeaderController"></header>

	<section class="wrapper" ng-view></section>

</body>
</html>
