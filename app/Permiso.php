<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permiso extends Model
{
    //
    use SoftDeletes;

    protected $table = 'permisos';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idPermiso';

    public function modulo()
    {
        return $this->belongsTo("App\Modulo", "idModulo", "idModulo");
    }

    public function usuario()
    {
        return $this->belongsTo("App\User", "idUsuario", "id");
    }
}
