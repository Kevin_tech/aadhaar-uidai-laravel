<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get("/", function () {
    return view("login");
});

Route::get("/app", function () {
    return view("index");
});

Route::get("/login", function () {
    return view("login");
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(["middleware" => ["web"]], function () {
    //
});

Route::group(["prefix" => "api"], function()
{
	// Authentication
	Route::get("authenticate", "AuthenticateController@getAuthenticatedUser");
	Route::post("authenticate/login", "AuthenticateController@authenticate");
	
	Route::get("documento/{documento}/imagen", "DocumentoController@showImage");

	// Password User
	Route::post("password", "AuthenticateController@updateUserPassword");
	Route::get("password/{usuario}", "AuthenticateController@resetUserPassword");
	Route::put("password/{usuario}", "AuthenticateController@updateUserPasswordAdmin");

	// Transactionals
	Route::resource("biometrico", "BiometricoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("ciudad", "CiudadController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("estado", "EstadoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("estadocivil", "EstadoCivilController",
					["only" => ["destroy", "index", "show"]]);
	Route::resource("genero", "GeneroController",
					["only" => ["destroy", "index", "show"]]);
	Route::resource("idioma", "IdiomaController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("inscripcion", "InscripcionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("modulo", "ModuloController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("permiso", "PermisoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("region", "RegionController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("residente", "ResidenteController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	Route::resource("tipobiometrico", "TipoBiometricoController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
	
	// Usuario
	Route::resource("usuario", "AuthenticateController",
					["only" => ["destroy", "index", "show", "store", "update"]]);
});

Route::group(["prefix" => "article"], function()
{
	Route::get("biometrico", function () {
		return view("article.biometrico");
	});

	Route::get("idioma", function () {
		return view("article.idioma");
	});

	Route::get("inscripcion", function () {
		return view("article.inscripcion");
	});

	Route::get("modulo", function () {
		return view("article.modulo");
	});

	Route::get("permiso", function () {
		return view("article.permiso");
	});

	Route::get("residente", function () {
		return view("article.residente");
	});

	Route::get("tipobiometrico", function () {
		return view("article.tipobiometrico");
	});

	Route::get("usuario", function () {
		return view("article.usuario");
	});
});

Route::group(["prefix" => "form"], function()
{
	Route::get("biometrico", function () {
		return view("form.biometrico");
	});

	Route::get("idioma", function () {
		return view("form.idioma");
	});

	Route::get("inscripcion", function () {
		return view("form.inscripcion");
	});

	Route::get("modulo", function () {
		return view("form.modulo");
	});

	Route::get("permiso", function () {
		return view("form.permiso");
	});

	Route::get("residente", function () {
		return view("form.residente");
	});

	Route::get("tipobiometrico", function () {
		return view("form.tipobiometrico");
	});

	Route::get("usuario", function () {
		return view("form.usuario");
	});
});

Route::group(["prefix" => "list"], function()
{
	Route::get("usuario", function () {
		return view("list.usuario");
	});
});

Route::group(["prefix" => "module"], function()
{
	Route::get("idiomas", function () {
		return view("module.idiomas");
	});

	Route::get("modulos", function () {
		return view("module.modulos");
	});

	Route::get("permisos", function () {
		return view("module.permisos");
	});

	Route::get("residentes", function () {
		return view("module.residentes");
	});

	Route::get("dashboard", function () {
		return view("module.dashboard");
	});

	Route::get("header", function () {
		return view("module.header");
	});

	Route::get("logout", function () {
		return view("module.logout");
	});

	Route::get("reportesresidentes", function () {
		return view("module.reportesresidentes");
	});

	Route::get("reportestelefonos", function () {
		return view("module.reportestelefonos");
	});

	Route::get("usuarios", function () {
		return view("module.usuarios");
	});
});

Route::group(["prefix" => "report"], function()
{
	Route::get("/operaciones", "ReporteController@reporteOperaciones");

	Route::get("/procedimiento", "ReporteController@reporteProcedimiento");

	Route::get("/pagocheque", "ReporteController@reportePagoCheque");
});