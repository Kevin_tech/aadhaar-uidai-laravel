<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DetalleCuenta;

class DetalleCuentaController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		
	}

	/*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	$record = DetalleCuenta::find( $id );

    	if( $record )
    	{
    		$record->deleted_at	= time();
    		$record->save();

    		if( $record->trashed() )
    		{
    			$response = response()->json([
					"msg"		=> "Record deleted",
					"id"		=> $id
				], 200);
    		}
    		else
    		{
    			$response = response()->json([
					"msg"		=> "Error",
					"id"		=> $id
				], 400);
    		}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }
	
    /*
	** LIST OF ALL RECORDS
    */
    public function index()
    {
        $records = DetalleCuenta::all();

    	if( count($records) > 0 )
    	{
    		$response = response()->json([
				"msg"		=> "All records",
				"records"	=> $records->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Zero records",
				"records"	=> Array()
			], 200);
    	}

    	return $response;
    }

    /*
	** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
    	$record = DetalleCuenta::find( $id );

    	if( $record )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
    		"id_cargo" => "required|integer",
            "id_ciclo" => "required|integer",
            "id_contrato" => "required|integer",
            "id_cuenta" => "required|integer",
            "id_numero_telefono" => "required|integer",
            "id_plan" => "required|integer",
            "id_promocion" => "required|integer",
            "id_telefono" => "required|integer",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Now insert
    	$record = new DetalleCuenta();
    	$record->id_cargo = $request->id_cargo;
        $record->id_ciclo = $request->id_ciclo;
        $record->id_contrato = $request->id_contrato;
        $record->id_cuenta = $request->id_cuenta;
        $record->id_numero_telefono = $request->id_numero_telefono;
        $record->id_plan = $request->id_plan;
        $record->id_promocion = $request->id_promocion;
        $record->id_telefono = $request->id_telefono;
    	
    	if( $record->save() )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
	** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
    		"id_cargo" => "required|integer",
            "id_ciclo" => "required|integer",
            "id_contrato" => "required|integer",
            "id_cuenta" => "required|integer",
            "id_numero_telefono" => "required|integer",
            "id_plan" => "required|integer",
            "id_promocion" => "required|integer",
            "id_telefono" => "required|integer",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Get the record correspond to $id
    	$record = DetalleCuenta::find( $id );

    	// If exists so update the data! Otherwise return 404
    	if( $record )
    	{
    		$record->id_cargo = $request->id_cargo;
            $record->id_ciclo = $request->id_ciclo;
            $record->id_contrato = $request->id_contrato;
            $record->id_cuenta = $request->id_cuenta;
            $record->id_numero_telefono = $request->id_numero_telefono;
            $record->id_plan = $request->id_plan;
            $record->id_promocion = $request->id_promocion;
            $record->id_telefono = $request->id_telefono;
	    	
	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }
}
