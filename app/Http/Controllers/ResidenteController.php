<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Residente;

class ResidenteController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		
	}

	public function destroy( $id )
    {
        $record = Residente::find( $id );

        if( $record )
        {
            $record->deleted_at = time();
            $record->save();

            if( $record->trashed() )
            {
                $response = response()->json([
                    "msg"       => "Record deleted",
                    "id"        => $id
                ], 200);
            }
            else
            {
                $response = response()->json([
                    "msg"       => "Error",
                    "id"        => $id
                ], 400);
            }
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "id"        => $id
            ], 404);
        }

        return $response;
    }
    
    /*
    ** LIST OF ALL RECORDS
    */
    public function index( Request $request )
    {
        $records = Residente::with("ciudad", "estadocivil", "genero", "idioma");

        if( $request->idCiudad )
            $records = $records->where("idCiudad", $request->idCiudad);

        $records = $records->get();

        if( count($records) > 0 )
        {
            $response = response()->json([
                "msg"       => "All records",
                "records"   => $records->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Zero records",
                "records"   => Array()
            ], 200);
        }

        return $response;
    }

    /*
    ** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
        $record = Residente::find( $id );

        if( $record )
        {
            $response = response()->json([
                "msg"       => "Success",
                "record"    => $record->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "record"    => Array()
            ], 404);
        }

        return $response;
    }

    private function uniqueAadhaar()
    {
        $aadhaarId = str_pad(mt_rand(0, 999999999999), 12, '0', STR_PAD_LEFT);
        $records = Residente::where("aadhaarId", $aadhaarId)->get();
        if( count($records) == 0 )
            return str_pad(mt_rand(0, 999999999999), 12, '0', STR_PAD_LEFT);
        $this->uniqueAadhaar();
    }

    /*
    ** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
        // Validator first!
        $validator = Validator::make($request->all(), [
            "idCiudad" => "required|integer|exists:ciudades",
            "idGenero" => "required|integer|exists:generos",
            "idIdioma" => "required|integer|exists:idiomas",
            "idEstadoCivil" => "required|integer|exists:estadocivil",
            "direccion" => "required|max:250",
            "fechaNacimiento" => "required|max:250",
            "nombre1" => "required|max:100",
            "nombre2" => "required|max:100",
            "nombre3" => "max:100",
            "apellido1" => "required|max:100",
            "apellido2" => "required|max:100",
            "apellido3" => "max:100",
        ]);

        if( $validator->fails() )
        {
            return response()->json([
                "msg"       => "Error, invalid data",
                "errors"    => $validator->errors()
            ], 400);
        }

        // Now insert
        $record = new Residente();

        $record->idCiudad = $request->idCiudad;
        $record->idGenero = $request->idGenero;
        $record->idIdioma = $request->idIdioma;
        $record->idEstadoCivil = $request->idEstadoCivil;
        $record->aadhaarId = $this->uniqueAadhaar();
        $record->direccion = $request->direccion;
        $record->fechaNacimiento = $request->fechaNacimiento;
        $record->nombre1 = $request->nombre1;
        $record->nombre2 = $request->nombre2;
        $record->nombre3 = $request->nombre3;
        $record->apellido1 = $request->apellido1;
        $record->apellido2 = $request->apellido2;
        $record->apellido3 = $request->apellido3;

        if( $record->save() )
        {
            $response = response()->json([
                "msg"       => "Success",
                "record"    => $record->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Error",
                "record"    => Array()
            ], 400);
        }

        return $response;
    }

    /*
    ** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
        // Validator first!
        $validator = Validator::make($request->all(), [
            "idCiudad" => "required|integer|exists:ciudades",
            "idGenero" => "required|integer|exists:generos",
            "idIdioma" => "required|integer|exists:idiomas",
            "idEstadoCivil" => "required|integer|exists:estadocivil",
            "direccion" => "required|max:250",
            "fechaNacimiento" => "required|max:250",
            "nombre1" => "required|max:100",
            "nombre2" => "required|max:100",
            "nombre3" => "required|max:100",
            "apellido1" => "required|max:100",
            "apellido2" => "required|max:100",
            "apellido3" => "required|max:100",
        ]);

        if( $validator->fails() )
        {
            return response()->json([
                "msg"       => "Error, invalid data",
                "errors"    => $validator->errors()
            ], 400);
        }

        // Get the record correspond to $id
        $record = Residente::find( $id );

        // If exists so update the data! Otherwise return 404
        if( $record )
        {
            $record->idCiudad = $request->idCiudad;
            $record->idGenero = $request->idGenero;
            $record->idIdioma = $request->idIdioma;
            $record->idEstadoCivil = $request->idEstadoCivil;
            $record->direccion = $request->direccion;
            $record->fechaNacimiento = $request->fechaNacimiento;
            $record->nombre1 = $request->nombre1;
            $record->nombre2 = $request->nombre2;
            $record->nombre3 = $request->nombre3;
            $record->apellido1 = $request->apellido1;
            $record->apellido2 = $request->apellido2;
            $record->apellido3 = $request->apellido3;

            if( $record->save() )
            {
                $response = response()->json([
                    "msg"       => "Success",
                    "record"    => $record->toArray()
                ], 200);
            }
            else
            {
                $response = response()->json([
                    "msg"       => "Error",
                    "record"    => Array()
                ], 400);
            }
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "record"    => Array()
            ], 404);
        }

        return $response;
    }
}
