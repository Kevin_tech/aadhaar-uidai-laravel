<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\DetalleFactura;

class DetalleFacturaController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		
	}

	public function destroy( $id )
    {
        $record = DetalleFactura::find( $id );

        if( $record )
        {
            $record->deleted_at = time();
            $record->save();

            if( $record->trashed() )
            {
                $response = response()->json([
                    "msg"       => "Record deleted",
                    "id"        => $id
                ], 200);
            }
            else
            {
                $response = response()->json([
                    "msg"       => "Error",
                    "id"        => $id
                ], 400);
            }
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "id"        => $id
            ], 404);
        }

        return $response;
    }
    
    /*
    ** LIST OF ALL RECORDS
    */
    public function index()
    {
        $records = DetalleFactura::all();

        if( count($records) > 0 )
        {
            $response = response()->json([
                "msg"       => "All records",
                "records"   => $records->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Zero records",
                "records"   => Array()
            ], 200);
        }

        return $response;
    }

    /*
    ** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
        $record = DetalleFactura::find( $id );

        if( $record )
        {
            $response = response()->json([
                "msg"       => "Success",
                "record"    => $record->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "record"    => Array()
            ], 404);
        }

        return $response;
    }

    /*
    ** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
        // Validator first!
        $validator = Validator::make($request->all(), [
            "id_cargo" => "required|integer",
            "id_dtrafico" => "required|integer",
            "id_factura" => "required|integer",
            "id_numero_telefono" => "required|integer",
            "id_promocion" => "required|integer",
            "monto_cargo" => "required|numeric",
            "monto_trafico" => "required|numeric",
        ]);

        if( $validator->fails() )
        {
            return response()->json([
                "msg"       => "Error, invalid data",
                "errors"    => $validator->errors()
            ], 400);
        }

        // Now insert
        $record = new DetalleFactura();

        $record->id_cargo = $request->id_cargo;
        $record->id_dtrafico = $request->id_dtrafico;
        $record->id_factura = $request->id_factura;
        $record->id_numero_telefono = $request->id_numero_telefono;
        $record->id_promocion = $request->id_promocion;
        $record->monto_cargo = $request->monto_cargo;
        $record->monto_trafico = $request->monto_trafico;

        if( $record->save() )
        {
            $response = response()->json([
                "msg"       => "Success",
                "record"    => $record->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Error",
                "record"    => Array()
            ], 400);
        }

        return $response;
    }

    /*
    ** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
        // Validator first!
        $validator = Validator::make($request->all(), [
            "id_cargo" => "required|integer",
            "id_dtrafico" => "required|integer",
            "id_factura" => "required|integer",
            "id_numero_telefono" => "required|integer",
            "id_promocion" => "required|integer",
            "monto_cargo" => "required|numeric",
            "monto_trafico" => "required|numeric",
        ]);

        if( $validator->fails() )
        {
            return response()->json([
                "msg"       => "Error, invalid data",
                "errors"    => $validator->errors()
            ], 400);
        }

        // Get the record correspond to $id
        $record = DetalleFactura::find( $id );

        // If exists so update the data! Otherwise return 404
        if( $record )
        {
            $record->id_cargo = $request->id_cargo;
            $record->id_dtrafico = $request->id_dtrafico;
            $record->id_factura = $request->id_factura;
            $record->id_numero_telefono = $request->id_numero_telefono;
            $record->id_promocion = $request->id_promocion;
            $record->monto_cargo = $request->monto_cargo;
            $record->monto_trafico = $request->monto_trafico;

            if( $record->save() )
            {
                $response = response()->json([
                    "msg"       => "Success",
                    "record"    => $record->toArray()
                ], 200);
            }
            else
            {
                $response = response()->json([
                    "msg"       => "Error",
                    "record"    => Array()
                ], 400);
            }
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "record"    => Array()
            ], 404);
        }

        return $response;
    }
}
