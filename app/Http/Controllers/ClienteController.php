<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cliente;

class ClienteController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		
	}

	public function destroy( $id )
    {
        $record = Cliente::find( $id );

        if( $record )
        {
            $record->deleted_at = time();
            $record->save();

            if( $record->trashed() )
            {
                $response = response()->json([
                    "msg"       => "Record deleted",
                    "id"        => $id
                ], 200);
            }
            else
            {
                $response = response()->json([
                    "msg"       => "Error",
                    "id"        => $id
                ], 400);
            }
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "id"        => $id
            ], 404);
        }

        return $response;
    }
    
    /*
    ** LIST OF ALL RECORDS
    */
    public function index( Request $request )
    {
        $records = Cliente::with("cuentas", "numerostelefono", "segmentocliente");

        if( $request->id_segmento_cliente )
            $records = $records->where("id_segmento", $request->id_segmento_cliente);
        
        if( $request->cantidad_cuentas )
            $records = $records->has("cuentas", ">=", $request->cantidad_cuentas);
        
        if( $request->cantidad_telefonos )
            $records = $records->has("numerostelefono", ">=", $request->cantidad_telefonos);
        
        $records = $records->get();

        if( count($records) > 0 )
        {
            $response = response()->json([
                "msg"       => "All records",
                "records"   => $records->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Zero records",
                "records"   => Array()
            ], 200);
        }

        return $response;
    }

    /*
    ** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
        $record = Cliente::find( $id );

        if( $record )
        {
            $response = response()->json([
                "msg"       => "Success",
                "record"    => $record->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "record"    => Array()
            ], 404);
        }

        return $response;
    }

    /*
    ** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
        // Validator first!
        $validator = Validator::make($request->all(), [
            "id_segmento" => "required|integer",
            "cui" => "required|max:15",
            "direccion_cliente" => "required|max:250",
            "nombre_cliente" => "required|max:250",
        ]);

        if( $validator->fails() )
        {
            return response()->json([
                "msg"       => "Error, invalid data",
                "errors"    => $validator->errors()
            ], 400);
        }

        // Now insert
        $record = new Cliente();

        $record->id_segmento = $request->id_segmento;
        $record->cui = $request->cui;
        $record->direccion_cliente = $request->direccion_cliente;
        $record->nombre_cliente = $request->nombre_cliente;

        if( $record->save() )
        {
            $response = response()->json([
                "msg"       => "Success",
                "record"    => $record->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Error",
                "record"    => Array()
            ], 400);
        }

        return $response;
    }

    /*
    ** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
        // Validator first!
        $validator = Validator::make($request->all(), [
            "id_segmento" => "required|integer",
            "cui" => "required|max:15",
            "direccion_cliente" => "required|max:250",
            "nombre_cliente" => "required|max:250",
        ]);

        if( $validator->fails() )
        {
            return response()->json([
                "msg"       => "Error, invalid data",
                "errors"    => $validator->errors()
            ], 400);
        }

        // Get the record correspond to $id
        $record = Cliente::find( $id );

        // If exists so update the data! Otherwise return 404
        if( $record )
        {
            $record->id_segmento = $request->id_segmento;
            $record->cui = $request->cui;
            $record->direccion_cliente = $request->direccion_cliente;
            $record->nombre_cliente = $request->nombre_cliente;

            if( $record->save() )
            {
                $response = response()->json([
                    "msg"       => "Success",
                    "record"    => $record->toArray()
                ], 200);
            }
            else
            {
                $response = response()->json([
                    "msg"       => "Error",
                    "record"    => Array()
                ], 400);
            }
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "record"    => Array()
            ], 404);
        }

        return $response;
    }
}
