<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Inscripcion;

class InscripcionController extends Controller
{
    public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		
	}

	public function destroy( $id )
    {
        $record = Inscripcion::find( $id );

        if( $record )
        {
            $record->deleted_at = time();
            $record->save();

            if( $record->trashed() )
            {
                $response = response()->json([
                    "msg"       => "Record deleted",
                    "id"        => $id
                ], 200);
            }
            else
            {
                $response = response()->json([
                    "msg"       => "Error",
                    "id"        => $id
                ], 400);
            }
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "id"        => $id
            ], 404);
        }

        return $response;
    }
    
    /*
    ** LIST OF ALL RECORDS
    */
    public function index( Request $request )
    {
        $records = Inscripcion::with("residente")->get();

        if( count($records) > 0 )
        {
            $response = response()->json([
                "msg"       => "All records",
                "records"   => $records->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Zero records",
                "records"   => Array()
            ], 200);
        }

        return $response;
    }

    /*
    ** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
        $record = Inscripcion::find( $id );

        if( $record )
        {
            $response = response()->json([
                "msg"       => "Success",
                "record"    => $record->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Not found",
                "record"    => Array()
            ], 404);
        }

        return $response;
    }

    /*
    ** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
        // Validator first!
        $validator = Validator::make($request->all(), [
            "idResidente" => "required|integer|exists:residentes",
            "idUsuario" => "required|integer|exists:usuarios",
        ]);

        if( $validator->fails() )
        {
            return response()->json([
                "msg"       => "Error, invalid data",
                "errors"    => $validator->errors()
            ], 400);
        }

        // Now insert
        $record = new Inscripcion();

        $record->idResidente = $request->idResidente;
        $record->idUsuario = $request->idUsuario;

        if( $record->save() )
        {
            $response = response()->json([
                "msg"       => "Success",
                "record"    => $record->toArray()
            ], 200);
        }
        else
        {
            $response = response()->json([
                "msg"       => "Error",
                "record"    => Array()
            ], 400);
        }

        return $response;
    }
}
