<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Hash;
use Validator;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends Controller
{
	public function __construct()
	{
		// Apply the jwt.auth middleware to all methods in this controller
		// except for the authenticate method. We don't want to prevent
		// the user from retrieving their token if they don't already have it
		$this->middleware("jwt.auth", ["except" => ["authenticate", "store"]]);
	}

	public function authenticate(Request $request)
    {
    	$credentials 	= $request->only("username", "password");
    	//$claims			= ["foo" => "bar", "baz" => "bob"];

		try
		{
			// verify the credentials and create a token for the user
			if (! $token = JWTAuth::attempt($credentials))
			{
				return response()->json(["error" => "Invalid credentials"], 401);
			}
		}
		catch (JWTException $e)
		{
			// something went wrong
			return response()->json(["error" => "Not authenticated"], 500);
		}

		// if no errors are encountered we can return a JWT
		return response()->json(compact("token"));
    }

    /*
	** SOFT DELETE A RECORD BY ID
    */
    public function destroy( $id )
    {
    	$record = User::find( $id );

    	if( $record )
    	{
    		$record->deleted_at	= time();
    		$record->save();

    		if( $record->trashed() )
    		{
    			$response = response()->json([
					"msg"		=> "Record deleted",
					"id"		=> $id
				], 200);
    		}
    		else
    		{
    			$response = response()->json([
					"msg"		=> "Error",
					"id"		=> $id
				], 400);
    		}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"id"		=> $id
			], 404);
    	}

    	return $response;
    }

    // somewhere in your controller
	public function getAuthenticatedUser()
	{
		try
		{
			if (! $user = JWTAuth::parseToken()->authenticate())
			{
				return response()->json(['user_not_found'], 404);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

		$response = response()->json([
			"msg"		=> "Success",
			"record"	=> $user
		], 200);

		return $response;
	}

    /*
	** LIST OF ALL RECORDS
    */
    public function index()
    {
    	$records = User::all();

    	if( count($records) > 0 )
    	{
    		$response = response()->json([
				"msg"		=> "All records",
				"records"	=> $records->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Zero records",
				"records"	=> Array()
			], 200);
    	}

    	return $response;
    }

    /*
	** RESET USER PASSWORD WITH JWT TOKEN VALIDATE
    */
    public function resetUserPassword( $id )
    {
    	try
		{
			if (! $usuario = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Unauthorized",
					"record"	=> Array()
				], 401);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

		if( $usuario->idrol == 1 )
		{
	    	$record = User::find( $id );

	    	if( $record )
	    	{
	    		$new_password = $record->usuario;
	    		$record->password = bcrypt( $new_password );

	    		if( $record->save() )
		    	{
		    		$response = response()->json([
						"msg"		=> "Success",
						"password"	=> $new_password
					], 200);
		    	}
		    	else
		    	{
		    		$response = response()->json([
						"msg"		=> "Error",
						"password"	=> ""
					], 400);
		    	}
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Not found",
					"password"	=> ""
				], 404);
	    	}
	    }
	    else
    	{
    		$response = response()->json([
				"msg"		=> "Forbidden",
				"password"	=> ""
			], 403);
    	}

    	return $response;
    }

    /*
	** SEARCH ONE RECORD BY PRIMARY KEY ID
    */
    public function show( $id )
    {
    	try
		{
			if (! $usuario = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Unauthorized",
					"record"	=> Array()
				], 401);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

    	$record = User::find( $id );

    	if( $record )
    	{
    		$record->you = $record->id == $usuario->id;
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** THIS METHOD INSERT NEW RECORD IN THE DATABASE
    */
    public function store( Request $request )
    {
    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"username" => "required|max:50|unique:usuarios",
			"password" => "required|max:50",
			"email" => "required|email|max:200",
			"nombre" => "required|max:200",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"request"	=> $request->all(),
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Now insert
    	$record = new User();
    	$record->password = bcrypt( $request->password );
    	$record->username = $request->username;
    	$record->email = $request->email;
    	$record->nombre = $request->nombre;

    	if( $record->save() )
    	{
    		$response = response()->json([
				"msg"		=> "Success",
				"record"	=> $record->toArray()
			], 200);
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Error",
				"record"	=> Array()
			], 400);
    	}

    	return $response;
    }

    /*
	** UPDATE AN EXISTING RECORD BY PK ID
    */
    public function update( Request $request, $id )
    {
		// Validator first!
    	$validator = Validator::make($request->all(), [
			"username" => "required|max:50|unique:usuarios",
			"password" => "required|max:50",
			"email" => "required|email|max:200",
			"nombre" => "required|max:200",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

    	// Get the record correspond to $id
    	$record = User::find( $id );

    	// If exists so update the data! Otherwise return 404
    	if( $record )
    	{
    		$record->password = bcrypt( $request->password );
    		$record->username = $request->username;
    		$record->email = $request->email;
    		$record->nombre = $request->nombre;

	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
    	}
    	else
    	{
    		$response = response()->json([
				"msg"		=> "Not found",
				"record"	=> Array()
			], 404);
    	}

    	return $response;
    }

    /*
	** UPDATE USER PASSWORD WITH JWT TOKEN VALIDATE
    */
    public function updateUserPassword( Request $request )
    {
    	try
		{
			if (! $record = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Unauthorized",
					"record"	=> Array()
				], 401);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

		// Validator first!
    	$validator = Validator::make($request->all(), [
			"oldpassword" => "required|max:50",
			"password" => "required|max:50|confirmed",
			"password_confirmation" => "required|max:50",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

		if( Hash::check($request->oldpassword, $record->password) )
		{
			$record->password 			= bcrypt( $request->password );

	    	if( $record->save() )
	    	{
	    		$response = response()->json([
					"msg"		=> "Success",
					"record"	=> $record->toArray()
				], 200);
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Error",
					"record"	=> Array()
				], 400);
	    	}
		}
		else
		{
			$response = response()->json([
				"msg"		=> "Current password not match",
				"record"	=> Array()
			], 400);
		}
    	
    	return $response;
    }

    /*
	** UPDATE USER PASSWORD WITH USER ID (ADMIN ONLY)
    */
    public function updateUserPasswordAdmin( Request $request, $id )
    {
    	try
		{
			if (! $usuario = JWTAuth::parseToken()->authenticate())
			{
				return response()->json([
					"msg"		=> "Unauthorized",
					"record"	=> Array()
				], 401);
			}
		}
		catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e)
		{
			return response()->json(['token_expired'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e)
		{
			return response()->json(['token_invalid'], $e->getStatusCode());
		}
		catch (Tymon\JWTAuth\Exceptions\JWTException $e)
		{
			return response()->json(['token_absent'], $e->getStatusCode());
		}

    	// Validator first!
    	$validator = Validator::make($request->all(), [
			"password" => "required|max:50|confirmed",
			"password_confirmation" => "required|max:50",
		]);

		if( $validator->fails() )
		{
			return response()->json([
				"msg"		=> "Error, invalid data",
				"errors"	=> $validator->errors()
			], 400);
		}

		if( $usuario->idrol == 1 )
		{
	    	// Get the record correspond to $id
	    	$record = User::find( $id );

	    	if( $record )
	    	{
	    		$record->password = $request->input("password", bcrypt( $record->password ));

	    		if( $record->save() )
		    	{
		    		$response = response()->json([
						"msg"		=> "Success",
						"record"	=> $record->toArray()
					], 200);
		    	}
		    	else
		    	{
		    		$response = response()->json([
						"msg"		=> "Error",
						"record"	=> Array()
					], 400);
		    	}
	    	}
	    	else
	    	{
	    		$response = response()->json([
					"msg"		=> "Not found",
					"record"	=> Array()
				], 404);
	    	}
	    }
	    else
    	{
    		$response = response()->json([
				"msg"		=> "Forbidden",
				"record"	=> Array()
			], 403);
    	}

    	return $response;
    }
}
