<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modulo extends Model
{
    //
    use SoftDeletes;

    protected $table = 'modulos';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idModulo';
}
