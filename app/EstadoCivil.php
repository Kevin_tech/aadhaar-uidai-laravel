<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoCivil extends Model
{
    //
    use SoftDeletes;

    protected $table = 'estadocivil';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idEstadoCivil';
}
