<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Idioma extends Model
{
    //
    use SoftDeletes;

    protected $table = 'idiomas';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idIdioma';
}
