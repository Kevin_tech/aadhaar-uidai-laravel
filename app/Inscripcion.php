<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inscripcion extends Model
{
    //
    use SoftDeletes;

    protected $table = 'inscripciones';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idInscripcion';

    public function residente()
    {
        return $this->belongsTo("App\Residente", "idResidente", "idResidente");
    }

    public function usuario()
    {
        return $this->belongsTo("App\User", "idUsuario", "id");
    }
}
