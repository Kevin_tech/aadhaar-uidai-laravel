<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estado extends Model
{
    //
    use SoftDeletes;

    protected $table = 'estados';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idEstado';

    public function region()
    {
        return $this->belongsTo("App\Region", "idRegion", "idRegion");
    }
}
