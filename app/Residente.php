<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Residente extends Model
{
    //
    use SoftDeletes;

    protected $table = 'residentes';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idResidente';

    public function ciudad()
    {
        return $this->belongsTo("App\Ciudad", "idCiudad", "idCiudad");
    }

    public function estadocivil()
    {
        return $this->belongsTo("App\EstadoCivil", "idEstadoCivil", "idEstadoCivil");
    }

    public function genero()
    {
        return $this->belongsTo("App\Genero", "idGenero", "idGenero");
    }

    public function idioma()
    {
        return $this->belongsTo("App\Idioma", "idIdioma", "idIdioma");
    }

    public function inscripciones()
    {
        return $this->hasMany("App\Inscripcion", "idResidente", "idResidente");
    }

	public function huellas()
    {
        return $this->hasMany("App\HuellaDactilar", "idResidente", "idResidente");
    }

    public function retina()
    {
        return $this->hasMany("App\Retina", "idResidente", "idResidente");
    }
}
