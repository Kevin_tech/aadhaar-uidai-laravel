<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoBiometrico extends Model
{
    //
    use SoftDeletes;

    protected $table = 'tipobiometricos';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idTipoBiometricos';
}
