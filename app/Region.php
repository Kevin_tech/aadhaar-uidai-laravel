<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    //
    use SoftDeletes;

    protected $table = 'regiones';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idRegion';
}
