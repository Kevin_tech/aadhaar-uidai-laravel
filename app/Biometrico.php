<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Biometrico extends Model
{
    //
    use SoftDeletes;

    protected $table = 'biometricos';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'idBiometrico';

    public function residente()
    {
        return $this->belongsTo("App\Residente", "idResidente", "idResidente");
    }

    public function tipobiometrico()
    {
        return $this->belongsTo("App\TipoBiometrico", "idTipoBiometrico", "idTipoBiometrico");
    }
}
