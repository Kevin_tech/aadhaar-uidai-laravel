var app = angular.module("appTracking", [ "ngStorage", "angular-loading-bar", "oitozero.ngSweetAlert" ])

.constant("urls",
{
	BASE: "/",
	BASE_API: "/api"
})

.config(["$httpProvider", "cfpLoadingBarProvider", function ($httpProvider, cfpLoadingBarProvider)
{
	$httpProvider.interceptors.push(["$q", "$location", "$localStorage", function ($q, $location, $localStorage) 
	{
		return {
			"request": function (config)
			{
				config.headers = config.headers || {};
				if ($localStorage.token)
				{
					config.headers.Authorization = "Bearer " + $localStorage.token;
				}
				
				return config;
			},
			"responseError": function (response)
			{
				if (response.status === 401 || response.status === 403)
				{
					$location.path("/login");
				}

				console.log( response );

				return $q.reject(response);
			}
		};
	}]);

	cfpLoadingBarProvider.includeSpinner = false;
}])

.controller("LoginController", ["$scope", "$window", "$localStorage", "Auth", "SweetAlert",
	function($scope, $window, $localStorage, Auth, SweetAlert)
	{
		$scope.credencial 	= {};
		$scope.usuarioRegistro 	= {};
		$scope.$storage		= $localStorage;
		$scope.loginform	= true;

		$scope.submit = function()
		{
			Auth.signin($scope.credencial, 
				function(data)
				{ 
					$scope.$storage.token = data.token;
					$window.location.href = "/app";
					SweetAlert.swal("¡Bienvenido!", "En breve serás redirigido al sistema", "success");
				},
				function(data)
				{
					SweetAlert.swal("Datos inválidos", "Parece que tu usuario o contraseña no son correctos, prueba de nuevo.", "error");
				});
		}

		$scope.register = function()
		{
			Auth.signup($scope.usuarioRegistro,
				function(data)
				{
					$scope.$storage.token = data.token;
					$window.location.href = "/app";
					SweetAlert.swal("¡Bienvenido!", "En breve serás redirigido al sistema", "success");
				},
				function(data)
				{
					SweetAlert.swal("Datos inválidos", 
						"Parece que tus no son correctos, prueba de nuevo.", "error");
				})
		}
	}
])

.factory("Auth", ["$http", "$localStorage", "urls", 
	function ($http, $localStorage, urls)
	{
		function urlBase64Decode(str)
		{
			var output = str.replace("-", "+").replace("_", "/");
			switch (output.length % 4)
			{
				case 0:
					break;
				case 2:
					output += "==";
					break;
				case 3:
					output += "=";
					break;
				default:
					throw "Illegal base64url string!";
			}

			return window.atob(output);
		}

		function getClaimsFromToken()
		{
			var token = $localStorage.token;
			var user = {};
			if (typeof token !== "undefined")
			{
				var encoded = token.split(".")[1];
				user = JSON.parse(urlBase64Decode(encoded));
			}

			return user;
		}

		var tokenClaims = getClaimsFromToken();

		return {
			signin: function (data, success, error)
			{
				$http.post(urls.BASE_API + "/authenticate/login", data).success(success).error(error);
			},
			signup: function (data, success, error)
			{
				$http.post(urls.BASE_API + "/usuario", data).success(success).error(error)
			},
			logout: function (success)
			{
				tokenClaims = {};
				delete $localStorage.token;
				success();
			},
			getTokenClaims: function ()
			{
				return tokenClaims;
			}
		};
	}
])