var app = angular.module("appTracking", [ "angular-loading-bar", "file-model", "googlechart", "ngDialog", "ngResource", "ngRoute", "ngSanitize", "ngStorage", "oitozero.ngSweetAlert", "omr.directives", "simplePagination" ])

.config(["$routeProvider", "$httpProvider", "cfpLoadingBarProvider",
function ($routeProvider, $httpProvider, cfpLoadingBarProvider)
{
	$routeProvider
	.when("/idiomas",
	{
		controller: "IdiomaController",
		listview: true,
		templateUrl: "module/idiomas",
		title: "Idiomas"
	})
		.when("/idiomas/new",
		{
			controller: "IdiomaController",
			newform: true,
			templateUrl: "form/idioma",
			title: "Nuevo Idioma"
		})
		.when("/idiomas/:id",
		{
			controller: "IdiomaController",
			editform: true,
			templateUrl: "article/idioma",
			title: "Idioma"
		})
	.when("/modulos",
	{
		controller: "ModuloController",
		listview: true,
		templateUrl: "module/modulos",
		title: "Módulos"
	})
		.when("/modulos/new",
		{
			controller: "ModuloController",
			newform: true,
			templateUrl: "form/modulo",
			title: "Nuevo Módulo"
		})
		.when("/modulos/:id",
		{
			controller: "ModuloController",
			editform: true,
			templateUrl: "article/modulo",
			title: "Módulo"
		})
	.when("/permisos",
	{
		controller: "PermisoController",
		listview: true,
		templateUrl: "module/permisos",
		title: "Permisos"
	})
		.when("/permisos/new",
		{
			controller: "PermisoController",
			newform: true,
			templateUrl: "form/permiso",
			title: "Nuevo Permiso"
		})
		.when("/permisos/:id",
		{
			controller: "PermisoController",
			editform: true,
			templateUrl: "article/permiso",
			title: "Permiso"
		})
	.when("/residentes",
	{
		controller: "ResidenteController",
		listview: true,
		templateUrl: "module/residentes",
		title: "Residentes"
	})
		.when("/residentes/new",
		{
			controller: "ResidenteController",
			newform: true,
			templateUrl: "form/residente",
			title: "Nuevo Residente"
		})
		.when("/residentes/:id",
		{
			controller: "ResidenteController",
			editform: true,
			templateUrl: "article/residente",
			title: "Residente"
		})
	.when("/home",
	{
		controller: "HomeController",
		templateUrl: "module/dashboard",
		title: "Inicio"
	})
	.when("/logout",
	{
		controller: "LogoutController",
		templateUrl: "module/logout",
		title: "...Saliendo..."
	})
	.when("/reportes",
	{
		controller: "ReporteController",
		listview: true,
		templateUrl: "module/reportes",
		title: "Mis Reportes"
	})
	.when("/reportes/residentes",
	{
		controller: "ReporteController",
		listview: true,
		templateUrl: "module/reportesresidentes",
		title: "Mis Reportes de Residentes"
	})
	.when("/reportes/telefonos",
	{
		controller: "ReporteController",
		listview: true,
		templateUrl: "module/reportestelefonos",
		title: "Mis Reportes de Candidatos"
	})
	.when("/usuarios",
	{
		controller: "UsuarioController",
		listview: true,
		templateUrl: "module/usuarios",
		title: "Usuarios"
	})
		.when("/usuarios/new",
		{
			controller: "UsuarioController",
			newform: true,
			templateUrl: "form/usuario",
			title: "Nuevo Usuario"
		})
		.when("/usuarios/:id",
		{
			controller: "UsuarioController",
			editform: true,
			templateUrl: "article/usuario",
			title: "Usuario"
		})
	.otherwise({ redirectTo: "/home" });

	$httpProvider.interceptors.push(["$q", "$location", "$localStorage", "SweetAlert", function ($q, $location, $localStorage, SweetAlert) 
	{
		return {
			"request": function (config)
			{
				console.log( "Data: " );
				console.log( config.data  );

				config.headers = config.headers || {};
				if ($localStorage.token)
				{
					config.headers.Authorization = "Bearer " + $localStorage.token;
				}
				
				return config;
			},
			"responseError": function (response)
			{
				if( response.status === 400 )
				{
					var errors = null;

					if( response.data.errors != undefined )
						errors = Object.keys( response.data.errors )
									.toString()
									.replace(/id_/g, "")
									.replace(/_/g, " ");

					SweetAlert.swal("Error al guardar", "Revisa los siguientes campos: " + errors, "error");
				}
				else if (response.status === 401 || response.status === 403)
				{
					$location.path("/logout");
				}
				else if( response.status === -1 )
				{
					SweetAlert.swal("Sin conexión", "No es posible establecer conexión con el servidor", "error");
				}
				else
				{
					SweetAlert.swal("Error al guardar", "Revisa que no falte ningun dato importante y prueba de nuevo", "error");
				}

				console.log( response );

				return $q.reject(response);
			}
		};
	}]);

	$httpProvider.defaults.transformRequest = function(data)
	{
		if (data === undefined)
			return data;

		var fd = new FormData();
		angular.forEach(data, function(value, key)
		{
			if (value instanceof FileList)
			{
				if (value.length == 1)
				{
					fd.append(key, value[0]);
				}
				else
				{
					angular.forEach(value,
						function(file, index) 
						{
							fd.append(key + "_" + index, file);
						});
				}
			}
			else
			{
				fd.append(key, value);
			}
		});

		return fd;
	}

	$httpProvider.defaults.headers.post["Content-Type"] = undefined;

	cfpLoadingBarProvider.includeSpinner = false;
}])

.constant("urls",
{
	BASE: "/",
	BASE_API: "/api",
	BASE_REPORT: "/report",
	APP_TITLE: " :: Aadhaar Unique Identification System "
})

.controller("IdiomaController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "Idioma",
function($rootScope, $scope, $location, $routeParams, SweetAlert, Idioma)
{
	$scope.current_date = new Date();
	$scope.idioma = {};
	$scope.title = "Idioma";

	if( $scope.listview )
	{
		Idioma.get(function(data)
		{
			$scope.idiomas = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id

		Idioma.get({id:id}, function(data)
		{
			$scope.idioma = data.record;
		});
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				Idioma.save( $scope.idioma, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro agregado", "success");
					$location.path( "idiomas" );
				});
			}

			else if( $scope.editform )
			{
				Idioma.update( $scope.idioma, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro modificado", "success");
					$location.path( "idiomas" );
				});
			}
		}
}])

.controller("ModuloController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "Modulo",
function($rootScope, $scope, $location, $routeParams, SweetAlert, Modulo)
{
	$scope.current_date = new Date();
	$scope.modulo = {};
	$scope.title = "Modulo";

	if( $scope.listview )
	{
		Modulo.get(function(data)
		{
			$scope.modulos = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id

		Modulo.get({id:id}, function(data)
		{
			$scope.modulo = data.record;
		});
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				Modulo.save( $scope.modulo, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro agregado", "success");
					$location.path( "modulos" );
				});
			}

			else if( $scope.editform )
			{
				Modulo.update( $scope.modulo, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro modificado", "success");
					$location.path( "modulos" );
				});
			}
		}
}])

.controller("PermisoController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "Modulo", "Permiso", "Usuario",
function($rootScope, $scope, $location, $routeParams, SweetAlert, Modulo, Permiso, Usuario)
{
	$scope.current_date = new Date();
	$scope.permiso = {};
	$scope.title = "Permisos";

	if( $scope.listview )
	{
		Permiso.get(function(data)
		{
			$scope.permisos = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id

		Permiso.get({id:id}, function(data)
		{
			$scope.permiso = data.record;
		});
	}

	if( $scope.editform || $scope.newform )
	{
		Modulo.get(function(data)
		{
			$scope.modulos = data.records;
		});

		Usuario.get(function(data)
		{
			$scope.usuarios = data.records;
		});
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				Permiso.save( $scope.permiso, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro agregado", "success");
					$location.path( "permisos" );
				});
			}

			else if( $scope.editform )
			{
				Permiso.update( $scope.permiso, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro modificado", "success");
					$location.path( "permisos" );
				});
			}
		}
}])

.controller("ResidenteController", ["$rootScope", "$scope", "$filter", "$location", "$routeParams", "SweetAlert", "Ciudad", "Estado", "EstadoCivil", "Genero", "Idioma", "Region", "Residente",
function($rootScope, $scope, $filter, $location, $routeParams, SweetAlert, Ciudad, Estado, EstadoCivil, Genero, Idioma, Region, Residente)
{
	$scope.current_date = new Date();
	$scope.residente = {};
	$scope.title = "Residente";

	if( $scope.listview )
	{
		Residente.get(function(data)
		{
			$scope.residentes = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id

		Residente.get({id:id}, function(data)
		{
			$scope.residente = data.record;
		});
	}

	if( $scope.editform || $scope.newform )
	{
		Ciudad.get(function(data)
		{
			$scope.ciudades = data.records;
		});

		Estado.get(function(data)
		{
			$scope.estados = data.records;
		});

		EstadoCivil.get(function(data)
		{
			$scope.estadoCivil = data.records;
		});

		Genero.get(function(data)
		{
			$scope.generos = data.records;
		});

		Idioma.get(function(data)
		{
			$scope.idiomas = data.records;
		});

		Region.get(function(data)
		{
			$scope.regiones = data.records;
		});
	}

	$scope.submit = function()
		{
			var formatFechaNacimiento = $filter("date")($scope.residente.fechaNacimiento, "yyyy-MM-dd");
			$scope.residente.fechaNacimiento = formatFechaNacimiento;

			if( $scope.newform )
			{
				Residente.save( $scope.residente, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro agregado", "success");
					$location.path( "residentes" );
				});
			}

			else if( $scope.editform )
			{
				Residente.update( $scope.residente, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro modificado", "success");
					$location.path( "residentes" );
				});
			}
		}
}])

.controller("UsuarioController", ["$rootScope", "$scope", "$location", "$routeParams", "SweetAlert", "Usuario",
function($rootScope, $scope, $location, $routeParams, SweetAlert, Usuario)
{
	$scope.current_date = new Date();
	$scope.usuario = {};
	$scope.title = "Usuario";

	if( $scope.listview )
	{
		Usuario.get(function(data)
		{
			$scope.usuarios = data.records;
		});
	}

	else if( $scope.editform )
	{
		var id = $routeParams.id

		Usuario.get({id:id}, function(data)
		{
			$scope.usuario = data.record;
		});
	}

	$scope.submit = function()
		{
			if( $scope.newform )
			{
				Usuario.save( $scope.usuario, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro agregado", "success");
					$location.path( "usuarios" );
				});
			}

			else if( $scope.editform )
			{
				Usuario.update( $scope.usuario, function(data)
				{
					SweetAlert.swal("Operación exitosa", "Registro modificado", "success");
					$location.path( "usuarios" );
				});
			}
		}
}])

.controller("HeaderController", ["$rootScope", "$scope", "$location", "$window", "Auth",
function($rootScope, $scope, $location, $window, Auth)
{
	$scope.current_date = new Date();
}])

.controller("HomeController", ["$rootScope", "$scope",
function($rootScope, $scope)
{
	$rootScope.currentModule = "home";
	$scope.current_date = new Date();
	$scope.myChartObject = {};
	$scope.myChartObject.type = "Gauge";
	$scope.myChartObject.options = {
		width: 720,
		height: 300,
		redFrom: 90,
		redTo: 100,
		yellowFrom: 75,
		yellowTo: 90,
		minorTicks: 5
	};

	$scope.myChartObject.data = [
		['Label', 'Value'],
		['Inscripciones/Día', 80],
		['Residentes', 25]
	];
}])

.controller("LogoutController", ["$rootScope", "$scope", "$window", "$localStorage", "Auth",
function($rootScope, $scope, $window, $localStorage, Auth)
{
	Auth.logout(function()
	{
		$window.location.href = "login";
	});
}])

.controller("ReporteController", ["$rootScope", "$scope", "$filter", "$window", "$httpParamSerializer", "urls", "Ciudad", "Estado", "Region", "Residente",
function($rootScope, $scope, $filter, $window, $httpParamSerializer, urls, Ciudad, Estado, Region, Residente)
{
	$rootScope.currentModule = "reportes";
	$scope.current_date = new Date();

	Ciudad.get(function(data)
	{
		$scope.ciudades = data.records;
	});

	Estado.get(function(data)
	{
		$scope.estados = data.records;
	});

	Region.get(function(data)
	{
		$scope.regiones = data.records;
	});

	$scope.generateReportResidentes = function()
	{
		Residente.get($scope.reporteresidente, function(data)
		{
			$scope.residentes = data.records;
		})
	}

	$scope.generateReportTelefonos = function()
	{
		Telefono.get($scope.reportetelefono, function(data)
		{
			$scope.telefonos = data.records;
		})
	}

	$scope.generateReportOperaciones = function()
		{
			var params = $httpParamSerializer( $scope.reporte_operaciones );
			$window.open( urls.BASE_REPORT + "/operaciones?" + params );
		}
}])

.directive("apiSrc", ["$http", "$localStorage", function ($http, $localStorage)
{
	var directive =
	{
		link: link,
		restrict: "A"
	};
	return directive;

	function link(scope, element, attrs)
	{
		var requestConfig =
		{
			method: "GET",
			url: attrs.apiSrc,
			responseType: "arraybuffer",
			cache: "true",
			headers:
			{
				"Authorization": "Bearer " + $localStorage.token
			}
		};

		$http(requestConfig).success(function(data)
		{
			var arr = new Uint8Array(data);

			var raw = "";
			var i, j, subArray, chunk = 5000;
			for (i = 0, j = arr.length; i < j; i += chunk)
			{
				subArray = arr.subarray(i, i + chunk);
				raw += String.fromCharCode.apply(null, subArray);
			}

			var b64 = btoa(raw);
			attrs.$set("src", "data:image/jpeg;base64," + b64);
		});
	}
}])

.directive("stringToNumber", function()
{
	return {
		require: "ngModel",
		link: 
			function(scope, element, attrs, ngModel)
			{
				ngModel.$parsers.push(
					function(value)
					{
						return "" + value;
					}
				);
			
				ngModel.$formatters.push(
					function(value)
					{
						return parseFloat(value, 10);
					}
				);
			}
	}
})

.factory("Auth", ["$http", "$localStorage", "urls", function ($http, $localStorage, urls)
{
	function urlBase64Decode(str)
	{
		var output = str.replace("-", "+").replace("_", "/");
		switch (output.length % 4)
		{
			case 0:
				break;
			case 2:
				output += "==";
				break;
			case 3:
				output += "=";
				break;
			default:
				throw "Illegal base64url string!";
		}

		return window.atob(output);
	}

	function getClaimsFromToken()
	{
		var token = $localStorage.token;
		var user = {};
		if (typeof token !== "undefined")
		{
			var encoded = token.split(".")[1];
			user = JSON.parse(urlBase64Decode(encoded));
		}

		return user;
	}

	var tokenClaims = getClaimsFromToken();

	return {
		signup: function (data, success, error)
		{
			$http.post(urls.BASE + "/login", data).success(success).error(error)
		},
		signin: function (data, success, error)
		{
			$http.post(urls.BASE + "/login", data).success(success).error(error)
		},
		logout: function (success)
		{
			tokenClaims = {};
			delete $localStorage.token;
			success();
		},
		getTokenClaims: function ()
		{
			return tokenClaims;
		},
		getAuthenticatedUser: function(success)
		{
			$http.get(urls.BASE_API + "/authenticate").success(success);
		}
	};
}])

.factory("Biometrico", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/biometrico");
}])

.factory("Ciudad", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/ciudad");
}])

.factory("Estado", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/estado");
}])

.factory("EstadoCivil", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/estadocivil");
}])

.factory("Genero", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/genero");
}])

.factory("Idioma", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/idioma");
}])

.factory("Region", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/region");
}])

.factory("TipoBiometrico", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/tipobiometrico");
}])

.factory("Password", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/password");
}])

.factory("Modulo", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/modulo/:id", 
						{ 
							"id": "@id",
							"descripcion": "@descripcion",
							"nombre": "@nombre"
						}, 
						{
							"update": { method: "PUT" }
						});
}])

.factory("Permiso", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/permiso/:id", 
						{ 
							"id": "@id",
							"idUsuario": "@idUsuario",
							"idModulo": "@idModulo",
							"actualizar": "@actualizar",
							"eliminar": "@eliminar",
							"escribir": "@escribir",
							"leer": "@leer"
						}, 
						{
							"update": { method: "PUT" }
						});
}])

.factory("Residente", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/residente/:id", 
						{ 
							"id": "@id",
							"idGenero": "@idGenero",
							"idCiudad": "@idCiudad",
							"idIdioma": "@idIdioma",
							"idEstadoCivil": "@idEstadoCivil",
							"direccion": "@direccion",
							"fechaNacimiento": "@fechaNacimiento",
							"nombre1": "@nombre1",
							"nombre2": "@nombre2",
							"nombre3": "@nombre3",
							"apellido1": "@apellido1",
							"apellido2": "@apellido2",
							"apellido3": "@apellido3"
						}, 
						{
							"update": { method: "PUT" }
						});
}])

.factory("Reporte", ["$resource", "urls", function($http, urls)
{
	return {
		documento: function (data, success, error)
		{
			$http.post(urls.BASE_REPORT + "/documento", data).success(success).error(error)
		},
		expedientes: function (data, success, error)
		{
			$http.post(urls.BASE_REPORT + "/expedientes", data).success(success).error(error)
		},
		operaciones: function (data, success, error)
		{
			$http.post(urls.BASE_REPORT + "/operaciones", data).success(success).error(error)
		}
	};
}])

.factory("SegmentoCliente", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/segmentocliente/:id");
}])

.factory("Telefono", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/telefono/:id");
}])

.factory("Trafico", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/trafico/:id");
}])

.factory("Usuario", ["$resource", "urls", function($resource, urls)
{
	return $resource(urls.BASE_API + "/usuario/:id", { id: "@id" });
}])

.filter("abs", function ()
{
	return function(val) { return Math.abs(val); }
})

.filter("dotnotation", function ()
{
	return function(arr, property) { 
		var obj 		= {};

		for( item in arr ) // Loop the objects of the array
		{
			for( key in arr[item] ) // Loop the keys of the object
			{
				var num = parseInt(item) + 1;
				obj[ property + "." + num + "." + key ] = arr[item][key];
			}
		}

		return obj;
	}
})

.filter("go", ["$location", function ($location)
{
	return function(val) { $location.path( val ); }
}])

.filter("pad", function ()
{
	return function( num, size ) {
		var s = num + "";
		while (s.length < size) s = "0" + s;
		return s;
	};
})

.run(["$location", "$rootScope", "urls", function($location, $rootScope, urls)
{
	$rootScope.$on("$routeChangeSuccess", function (event, current, previous)
	{
		if( current.$$route !== undefined )
		{
			$rootScope.article 	= current.$$route.article;
			$rootScope.editform = current.$$route.editform;
			$rootScope.listview = current.$$route.listview;
			$rootScope.moduletitle = current.$$route.title;
			$rootScope.newform 	= current.$$route.newform;
			$rootScope.title 	= current.$$route.title + urls.APP_TITLE;
			$rootScope.showNotificacionDropdown = false;
			$rootScope.showUserDropdown 		= false;
		}
	});
}]);